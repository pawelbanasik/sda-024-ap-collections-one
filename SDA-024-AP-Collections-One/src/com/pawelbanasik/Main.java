package com.pawelbanasik;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		// List<Integer> myList = new ArrayList<>();
		
		
//		myList.add(1);
//		myList.add(2);
//		myList.add(4);
//		myList.add(2);
//		myList.add(5);
//		myList.add(12);
//		myList.add(13);
//		myList.add(2);

		// w jednej linii pisanie list!
		List<Integer> myList = Arrays.asList(1,2,4,2,5,12,13,2);
		int[] myArray = { 4, 2, 2, 1, 5, 29, 3, 8 };

		Compare compare1 = new Compare();

		System.out.println("Ilosc takich samych elementow " + compare1.check(myArray, myList));
	}

}
