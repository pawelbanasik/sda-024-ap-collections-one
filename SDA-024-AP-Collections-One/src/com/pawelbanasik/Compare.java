package com.pawelbanasik;

import java.util.List;

public class Compare {

	public int check(int[] myArray, List<Integer> myList) {

		int count = 0;
		for (int i = 0; i < myArray.length; i++) {

			if (myArray[i] == myList.get(i)) {

				count++;
			}
		}
		return count;
	}
}
