package com.pawelbanasik;

import java.util.Collection;
import java.util.List;

public class CollectionMath {

    public static int sum(Collection<Integer> numbers){
        int result = 0;
        for (int number : numbers) {
            result += number;
        }
        return result;
    }
}
